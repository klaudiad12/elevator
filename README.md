Ceci est un exercice simple qui a pour but de comprendre votre manière de penser et d’écrire du code. On vous demande donc de le faire par vos propres moyens.

Il n’y a pas de temps limite imposé et vous pouvez vous servir d’internet autant que vous voulez.

Commitez tous les changements que vous souhaitez et envoyez nous un email lorsque vous avez fini.

## Objectif 1 : Refactoring du code v1 (Obligatoire)

Nous allons travailler sur un projet de fonctionnement d’ascenseur pour un immeuble de 9 étages.

Le projet a déjà été initié et il est disponible à cette endroit : https://gitlab.com/-/ide/project/certeurope/elevator/edit/master/-/

Ce projet fonctionne mais il ne respecte pas l’intégralité du cahier des charges. Avant de le passer en production, il est nécessaire de le finaliser et de refactorer le code afin qu’il soit le plus propre, clair et optimisé possible.

Les tests dans le projet passent avec le code actuel et vous permettront de valider le fonctionnement du code après refactoring.

## Objectif 2 : Optimisation de l’algorithme (Facultatif)

Le fonctionnement est aujourd’hui à son point le plus simple.  Il n’y a pas d’optimisation de l’algorithme et il se contente de suivre les appels dans l’ordre.

Le but de cet exercice est de faire une v2 en améliorant ce fonctionnement pour que :

- L’ascenseur prenne des gens sur son passage uniquement s’il est en phase descendante.
- La phase descendante ou montante de la cabine est fixée par la 1ère instruction du json.
- Si l’ascenseur est en phase montante, il ne tiendra pas compte des instructions vers des étages inférieurs.
- Si l’ascenseur est en phase descendante, il ne tiendra pas compte des instructions vers des étages supérieurs.

## Cahier des charges

- L’ascenseur dessert tous les étages (de 0 à 9).
- Au lancement de chaque exécution, l’ascenseur démarre du niveau 0.

**Les entrées :**
- `fromFloor` est un appel de l’ascenseur depuis un étage. (Ex : `fromFloor` vaut 5 lorsqu’un appel est fait depuis le 5ème étage).
- `toFloor` est un ordre passé  à l’ascenseur depuis l’intérieur de la cabine. (Ex : `toFloor`  vaut 5 lorsque le bouton 5 de la cabine est pressé).
- L’API prend en entrée un fichier json. (Ex : https://gitlab.com/-/ide/project/certeurope/elevator/tree/master/-/src/test/resources/).

**La sortie :**
- Le retour de l’API décrira le parcours de l’ascenseur de la manière suivante :
    - `S` permet de montrer les arrêts de l’ascenseur.
    - `S-0-1-2-3-4-5-S` : Nous montre que l’ascenseur est en position Stop puis passe du niveau 0 au niveau 5 avant de se stopper.

## Exemples de fonctionnement du code v1

```json
[
  {
    "fromFloor": 0,
    "toFloor": 4
  }
]
```
Sortie : `S-0-1-2-3-4-S`

---

```json
[
  {
    "fromFloor": 0,
    "toFloor": 4
  },
  {
    "fromFloor": 4,
    "toFloor": 9
  }
]
```
Sortie : `S-0-1-2-3-4-S-4-5-6-7-8-9-S`

---

```json
[
  {
    "fromFloor": 8,
    "toFloor": 4
  },
  {
    "fromFloor": 5,
    "toFloor": 2
  }
]
```
Sortie : `S-0-1-2-3-4-5-6-7-8-S-8-7-6-5-4-S-4-5-S-5-4-3-2-S`

---

```json
[
  {
    "fromFloor": 0,
    "toFloor": 4
  },
  {
    "fromFloor": 4,
    "toFloor": 6
  },
  {
    "fromFloor": 4,
    "toFloor": 0
  }
]
```
Sortie : `S-0-1-2-3-4-S-4-5-6-S-6-5-4-S-4-3-2-1-0-S`
